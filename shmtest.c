#include <unistd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <signal.h>

#define SHM_UID 0xFEED

int8_t *BUFFER;
int    SHMID;

struct bufhead {
	int8_t finished;
	uint16_t elementsRead;
};

void cleanupshm() {
	shmdt(BUFFER);
	shmctl(SHMID, IPC_RMID, NULL);
}

void readyclean(void *buffer, int shmid) {
	BUFFER = buffer;
	SHMID = shmid;
}

void interrupt() {
	exit(0);
}

int main(int argc, char *argv[]) {
	// Set up shared memory
	int shmid, i;

	if ((shmid = shmget(SHM_UID, 4096, IPC_CREAT | IPC_EXCL | 0600)) < 0) {
		perror("Could not allocate shared memory, exiting");
		exit(1);
	}

	int8_t *buffer = shmat(shmid, NULL, 0);
	struct bufhead *bufhead = (struct bufhead*) buffer;
	buffer += sizeof(struct bufhead);

	// In case the program crashes or is sent a SIGINT
	readyclean(buffer, shmid);
	atexit(cleanupshm);

	signal(SIGINT, interrupt);

	if (fork() == 0) {
		// Read element
		// Increment elementsRead
		int currentPass;
		FILE *rand = fopen("/dev/random", "rb");
		if (!rand) { perror("Could not open file!\n"); exit(1); }

		for (i = 0; i < 10; i++) {
			currentPass = 0;
			do
				buffer += ((currentPass += fread(buffer, 1, 10 - currentPass, rand)));
			while (currentPass < 10);
			bufhead->elementsRead++;
		}
		
		bufhead->finished++;
	} else {
		// Ask the user for input
		// Start printing values

		char response[255];
		printf("Please enter a bitmask to use (255 will output raw, 0 will output all zeroes)\n");
		fgets(response, 255, stdin);
		int bitmask = atoi(response);
		int passes = 0, printed = 0;

		do {
			while (printed < bufhead->elementsRead) {
				for (i = 0; i < 10; i++) {
					printf("%02hhX ", *buffer & bitmask);
					buffer += 1;
				}
				printf("\n");

				printed++;
			}
			passes++;
			sleep(1);
		} while (!bufhead->finished);
		printf("Passes: %i\n", passes);
	}

	return 0;
}
